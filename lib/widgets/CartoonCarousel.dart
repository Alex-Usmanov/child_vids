import 'package:childvids2/screens/favourite_screen/bloc/favourite_bloc.dart' as favourite;
import 'package:childvids2/screens/search_screen/bloc/search_bloc.dart' as search;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CartoonCarousel extends StatefulWidget {
  final List<Widget> elements;

  const CartoonCarousel({Key key, @required this.elements}) : super(key: key);

  @override
  _CartoonCarouselState createState() =>
      _CartoonCarouselState(elements: this.elements);
}

class _CartoonCarouselState extends State<CartoonCarousel> {
  final List<Widget> elements;
  _CartoonCarouselState({this.elements});
  var bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<search.SearchBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollNotification>(
        onNotification: (scrollDetails) {
          if (scrollDetails.metrics.pixels ==
                  scrollDetails.metrics.maxScrollExtent &&
              bloc.isLoading == false) {
            bloc.add(search.Overscroll());
          }
          return false;
        },
        child: ListView(
          children: widget.elements,
        ));
  }
}

class FavouriteCartoonCarousel extends StatefulWidget {
  final List<Widget> elements;

  const FavouriteCartoonCarousel({Key key, @required this.elements}) : super(key: key);

  @override
  _FavouriteCartoonCarouselState createState() =>
      _FavouriteCartoonCarouselState(elements: this.elements);
}

class _FavouriteCartoonCarouselState extends State<FavouriteCartoonCarousel> {
  final List<Widget> elements;
  _FavouriteCartoonCarouselState({this.elements});
  var bloc;

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<favourite.FavouriteBloc>(context);
  }

    @override
  Widget build(BuildContext context) {
    return NotificationListener<ScrollNotification>(
        onNotification: (scrollDetails) {
          if (scrollDetails.metrics.pixels ==
                  scrollDetails.metrics.maxScrollExtent &&
              bloc.isLoading == false) {
            bloc.add(favourite.Overscroll());
          }
          return false;
        },
        child: ListView(
          children: widget.elements,
        ));
  }
}
import 'package:flutter/material.dart';

class DrawerItem extends StatelessWidget {
  final IconData icon;
  final String title;
  final Function onTap;
  final UniqueKey key = UniqueKey();

  DrawerItem({@required this.icon,@required this.title, @required this.onTap});

  @override
  Widget build(BuildContext context) {
    Color primaryColor = Theme.of(context).primaryColor;
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 5),

        height: 50,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(icon, color: Colors.grey,),
                SizedBox(width: 30,),
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                )
              ],
            ),
            Icon(Icons.arrow_forward_ios, color: Colors.grey,),
          ],
        ),
      ),
    );
  }
}

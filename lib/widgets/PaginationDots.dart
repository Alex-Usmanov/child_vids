import 'package:flutter/material.dart';

class ColorDot extends StatelessWidget {
  const ColorDot({Key key, this.color, this.borderColor, this.radius})
      : super(key: key);

  final Color color;
  final Color borderColor;
  final double radius;

  @override
  Widget build(BuildContext context) {
    Color color = this.color ?? Colors.white;
    Color borderColor = this.borderColor ?? Theme.of(context).primaryColor;
    double radius = this.radius ?? 8;

    return Padding(
      padding: const EdgeInsets.all(3),
      child: Container(
        height: radius,
        width: radius,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: color,
          border: Border.all(
            width: 0.8,
            color: borderColor,
          ),
        ),
      ),
    );
  }
}

class PaginationDots extends StatefulWidget {
  UniqueKey key = UniqueKey();
  int activeIndex;
  int totalDots;
  PaginationDots({@required this.activeIndex,@required this.totalDots});

  @override
  _PaginationDotsState createState() => _PaginationDotsState(activeIndex: activeIndex, totalDots: totalDots);
}

class _PaginationDotsState extends State<PaginationDots> {
  int activeIndex;
  int totalDots;

  void setActiveIndex(int index){
    setState(() {
      activeIndex = index;
    });
  }

  _PaginationDotsState({this.activeIndex, this.totalDots});

  List<Widget> buildDots(){
    List<Widget> result = new List<Widget>();
    for (int i = 0; i < totalDots; ++i){
      result.add(ColorDot(
        key: UniqueKey(),
        radius: 10.0,
        color: i == activeIndex ? Colors.white : Colors.transparent,
        borderColor: Colors.white,
      ));
    }
    return result;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 30,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: buildDots(),
      ),
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:childvids2/data_structures/Cartoon.dart';
import 'package:childvids2/services/db_service.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class FavouriteStar extends StatefulWidget {
  UniqueKey key = new UniqueKey();
  Cartoon cartoon;
  FavouriteStar({Key key, @required this.cartoon}) : super(key: key);

  @override
  _FavouriteStarState createState() => _FavouriteStarState(cartoon: cartoon);
}

class _FavouriteStarState extends State<FavouriteStar> {
  bool isFavourite = false;
  Cartoon cartoon;

  _FavouriteStarState({@required this.cartoon});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        setState(() {
          isFavourite = !isFavourite;
          DatabaseService.instance.addCartoon(cartoon);
          print('added ${cartoon.title}');
        });
      },
      icon: Icon(
        isFavourite ? Icons.star : Icons.star_border,
        color: Colors.yellow,
      ),
    );
  }
}

Widget buildCartoon(
    {@required BuildContext context, @required Cartoon cartoon, bool displayStar = true}) {
  return Material(
    elevation: 7.5,
    child: Container(
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
      ),
      child: InkWell(
        onTap: () {},
        splashColor: Theme.of(context).accentColor,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                CachedNetworkImage(
                  imageUrl: cartoon.profilePictureUrl,
                ),
                Container(
                    padding: EdgeInsets.only(left: 10),
                    width: 140,
                    child: Text(cartoon.title,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,

                        ),
                        overflow: TextOverflow.ellipsis)

                        ),
              ],
            ),
            Row(
              children: <Widget>[
                displayStar ? FavouriteStar(cartoon: cartoon) : Container(height: 0,width:0),
                Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.blueGrey,
                ),
              ],
            )
          ],
        ),
      ),
    ),
  );
}

List<Widget> buildCartoons(
    {@required BuildContext context, @required List<Cartoon> cartoons, bool displayStar = true}) {
  List<Widget> result = new List<Widget>();
  cartoons.forEach((current) {
    result.add(buildCartoon(cartoon: current, context: context, displayStar: displayStar));
  });
  return result;
}

import 'package:flutter/material.dart';

class WelcomeScreenTemplate extends StatelessWidget {
  final UniqueKey key = UniqueKey();
  final String title;
  final String subtitle;

  WelcomeScreenTemplate({@required this.title,@required this.subtitle});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,

        children: <Widget>[
        Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 35,
          ),
        ),
        SizedBox(
          height: 70,
        ),
        Text(
          subtitle,
          textAlign: TextAlign.center,
          style: TextStyle(color: Colors.white, fontSize: 20.0),
        ),
        ],
      ),
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0xFFF2994A),
                Color(0xFFF2C94C),
              ]
          )
      ),
    );
  }
}

import 'package:childvids2/screens/ExploreScreen.dart';
import 'package:childvids2/screens/favourite_screen/FavouriteCartoonsScreen.dart';
import 'package:childvids2/screens/search_screen/SearchScreen.dart';
import 'package:childvids2/screens/SettingsScreen.dart';
import 'package:flutter/material.dart';

import 'DrawerItem.dart';

class NavigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color primaryColor = Theme.of(context).primaryColor;
    Color accentColor = Theme.of(context).accentColor;

    return Drawer(
      child: Column(
        children: <Widget>[
          DrawerHeader(
            margin: EdgeInsets.only(bottom: 10),
            padding: EdgeInsets.all(0),
            child: Stack(
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [primaryColor, accentColor],
                  )),
                ),
                Positioned(
                  bottom: 10,
                  left: 10,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Material(
                          elevation: 5,
                          child: Image.asset(
                            'images/blank_profile.png',
                            width: 70,
                            height: 70,
                          )),
                      SizedBox(
                        height: 15,
                      ),
                      Text(
                        'Alex Usmanov',
                        style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 20,
                            color: Colors.white),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          DrawerItem(
            icon: Icons.favorite,
            title: 'Favourite',
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => FavouriteCartoonsScreen()));
            },
          ),
          DrawerItem(
            icon: Icons.explore,
            title: 'Explore',
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ExploreScreen()));
            },
          ),
          DrawerItem(
            icon: Icons.search,
            title: 'Search',
            onTap: () {
              print('trigger');
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SearchScreen()));
            },
          ),
          DrawerItem(
            icon: Icons.settings,
            title: 'Settings',
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SettingsScreen()));
            },
          ),
        ],
      ),
    );
  }
}

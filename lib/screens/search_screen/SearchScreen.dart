import 'dart:ui';

import 'package:childvids2/screens/search_screen/bloc/search_bloc.dart';
import 'package:childvids2/widgets/CartoonCarousel.dart';
import 'package:childvids2/widgets/CartoonBuilder.dart';
import 'package:childvids2/widgets/NavigationDrawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final bloc = new SearchBloc();

  @override
  void dispose() {
    super.dispose();
    bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: BlocProvider(child: SearchField(), create: (context) => bloc),
        ),
        body: BlocBuilder<SearchBloc, SearchState>(
          bloc: bloc,
          builder: (context, state) {
            if (state is InitialSearchState) {
              return Container(
                height: 0,
                width: 0,
              );
            } else if (state is LoadingSearchState) {
              List<Widget> widgets =
                  buildCartoons(context: context, cartoons: bloc.toons);
              widgets.add(Center(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: CircularProgressIndicator(),
                ),
              ));
              return BlocProvider(
                create: (context) => bloc,
                child: CartoonCarousel(
                  elements: widgets,
                ),
              );
            } else if (state is CompleteSearchState) {
              List<Widget> widgets =
                  buildCartoons(context: context, cartoons: bloc.toons);
              return BlocProvider(
                create: (context) => bloc,
                child: CartoonCarousel(
                  elements: widgets,
                ),
              );
            }
            return Container(
              height: 0,
              width: 0,
            );
          },
        ),
        drawer: NavigationDrawer(),
      ),
    );
  }
}

class SearchField extends StatefulWidget {
  const SearchField({
    Key key,
  }) : super(key: key);

  @override
  _SearchFieldState createState() => _SearchFieldState();
}

class _SearchFieldState extends State<SearchField> {
  var bloc;

  @override
  void dispose() {
    super.dispose();
    bloc.close();
  }

  @override
  void initState() {
    super.initState();
    bloc = BlocProvider.of<SearchBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Material(
        elevation: 0,
        child: TextField(
          onSubmitted: (input) {
            bloc.add(FormSubmit(searchQuery: input));
          },
          decoration: InputDecoration(
            filled: true,
            fillColor: Colors.white,
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: Colors.white)),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: Colors.white)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
                borderSide: BorderSide(color: Colors.white)),
            prefixIcon: Icon(
              Icons.search,
              color: Colors.blueGrey,
            ),
            hintText: 'Search...',
          ),
        ));
  }
}
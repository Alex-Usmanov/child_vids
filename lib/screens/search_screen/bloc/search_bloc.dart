import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:childvids2/data_structures/Cartoon.dart';
import 'package:childvids2/services/api_service.dart';
import 'package:meta/meta.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  String searchQuery;
  bool isLoading;
  List<Cartoon> toons;

  Future<List<Cartoon>> _fetchCartoons() async {
    List<Cartoon> loaded = new List<Cartoon>();
    isLoading = true;
    loaded = await APIService.instance.searchCartoons(query: searchQuery);
    isLoading = false;
    return loaded;
  }

  Future<void> loadMore() async {
    List<Cartoon> more = await _fetchCartoons();
    print('length: ${toons.length}');
    toons = toons..addAll(more);
    print('after: ${toons.length}');
  }

  @override
  SearchState get initialState => InitialSearchState();

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    if (event is FormSubmit) {
      print('nullin');
      toons = List<Cartoon>();
      searchQuery = event.searchQuery;
      yield LoadingSearchState();

      // Get first batch from api
      toons = await _fetchCartoons();
      yield CompleteSearchState(cartoons: toons);
    } else if (event is Overscroll) {
      yield LoadingSearchState();
      List<Cartoon> more = await _fetchCartoons();
      toons = toons..addAll(more);
    }
  }
}

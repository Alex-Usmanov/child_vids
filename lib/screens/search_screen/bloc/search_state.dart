part of 'search_bloc.dart';

@immutable
abstract class SearchState {}

class InitialSearchState extends SearchState {

}

class LoadingSearchState extends SearchState{

}

class CompleteSearchState extends SearchState{
  final List<Cartoon> cartoons;
  CompleteSearchState({this.cartoons});
}

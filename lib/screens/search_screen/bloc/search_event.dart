part of 'search_bloc.dart';

@immutable
abstract class SearchEvent {}

class FormSubmit extends SearchEvent {
  final String searchQuery;
  FormSubmit({this.searchQuery});
}

class Overscroll extends SearchEvent {}

import 'package:childvids2/widgets/NavigationDrawer.dart';
import 'package:flutter/material.dart';

class ExploreScreen extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Color primaryColor = Theme.of(context).primaryColor;
    Color accentColor = Theme.of(context).accentColor;

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          title: Text('Explore'),
          backgroundColor: Colors.white,
        ),
        drawer: NavigationDrawer(),
        body: Row(
          children: <Widget>[
            Expanded(
              child: Container(
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

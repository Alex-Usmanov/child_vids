import 'dart:ui';

import 'package:childvids2/screens/favourite_screen/FavouriteCartoonsScreen.dart';
import 'package:childvids2/widgets/WelcomeScreenTemplate.dart';
import 'package:childvids2/widgets/PaginationDots.dart';
import 'package:flutter/material.dart';

List<Widget> welcomeScreens = [
  WelcomeScreenTemplate(
    title: 'Welcome to Child Vids',
    subtitle:
        'An app showing cartoons that both entertain and develop your child',
  ),
  WelcomeScreenTemplate(
    title: 'Child-friendly content',
    subtitle: 'We won\'t show your child inapropriate video materials',
  ),
  WelcomeScreenTemplate(
    title: 'Limited Watching',
    subtitle:
        'We will not let your child to watch videos all day long. You can specify the watching time in the settings',
  ),
  WelcomeScreenTemplate(
    title: 'Parent control',
    subtitle:
        'Set up parent password so that only you could change settings! You can also block certain cartoons if you feel they do not fit your child',
  ),
];

class WelcomeScreen extends StatefulWidget {
  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  PageController _controller = PageController(
    initialPage: 0,
  );
  static int activeIndex = 0;

  void nextPage() {
    setState(() {
      activeIndex += 1;
    });
    _controller.animateToPage(activeIndex,
        duration: Duration(milliseconds: 600), curve: Curves.easeInOut);
    print('called $activeIndex');
  }

  void toMainScreen({BuildContext context}) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => FavouriteCartoonsScreen()));
  }

  @override
  Widget build(BuildContext context) {
    bool isLastPage = activeIndex == (welcomeScreens.length - 1);
    return SafeArea(
      child: Scaffold(
        floatingActionButton: FloatingActionButton(
          onPressed: isLastPage ? () => toMainScreen(context: context) : nextPage ,
          backgroundColor: Colors.white,
          child: Icon(
            isLastPage
                ? Icons.check
                : Icons.arrow_forward_ios,
            color: Colors.black,
          ),
        ),
        body: Stack(
          children: <Widget>[
            PageView(
              controller: _controller,
              children: welcomeScreens,
              onPageChanged: (index) {
                setState(() {
                  activeIndex = index;
                });
              },
            ),
            Align(
              child: PaginationDots(
                activeIndex: activeIndex,
                totalDots: welcomeScreens.length,
              ),
              alignment: Alignment.bottomCenter,
            )
          ],
        ),
      ),
    );
  }
}

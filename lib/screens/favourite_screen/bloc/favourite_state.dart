part of 'favourite_bloc.dart';

@immutable
abstract class FavouriteState {
}

class FavouriteInitial extends FavouriteState{
  
}

class FavouriteLoading extends FavouriteState {
  final List<Cartoon> cartoons;
  FavouriteLoading({@required this.cartoons});
}

class FavouriteComplete extends FavouriteState {
  final List<Cartoon> cartoons;
  FavouriteComplete({@required this.cartoons});
}

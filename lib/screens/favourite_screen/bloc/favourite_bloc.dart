import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:childvids2/data_structures/Cartoon.dart';
import 'package:childvids2/services/db_service.dart';
import 'package:meta/meta.dart';

part 'favourite_event.dart';
part 'favourite_state.dart';

class FavouriteBloc extends Bloc<FavouriteEvent, FavouriteState> {
  List<Cartoon> favourite = new List<Cartoon>();
  int loaded = 0;
  final int cartoonsPerBatch = 10;
  bool isLoading;

  Future<List<Cartoon>> _fetchCartoons(String table, int offset) async{
    print('aaaaa');
    List<Cartoon> cartoons =  await DatabaseService.instance.fetchBatch(table, offset, cartoonsPerBatch);
    loaded += cartoonsPerBatch;
    isLoading = false;
    return cartoons;
  }

  @override
  Stream<FavouriteState> mapEventToState(
    FavouriteEvent event,
  ) async* {
    if (event is OnLoad){
      // Return empty list
      yield FavouriteLoading(cartoons: favourite);

      // Load the first batch
      favourite = await _fetchCartoons('Favourite', 0);
      yield FavouriteComplete(cartoons: favourite);
    }
    else if (event is Overscroll){
      yield FavouriteLoading(cartoons: favourite);

      // Load next batch
      List<Cartoon> more = await _fetchCartoons('Favourite', loaded+cartoonsPerBatch);
      favourite = favourite..addAll(more);
      yield FavouriteComplete(cartoons: favourite);
    }
  }

  @override
  FavouriteState get initialState => FavouriteInitial();
}

import 'package:childvids2/data_structures/Cartoon.dart';
import 'package:childvids2/screens/favourite_screen/bloc/favourite_bloc.dart';
import 'package:childvids2/widgets/CartoonBuilder.dart';
import 'package:childvids2/widgets/CartoonCarousel.dart';
import 'package:childvids2/widgets/NavigationDrawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FavouriteCartoonsScreen extends StatefulWidget {
  @override
  _FavouriteCartoonsScreenState createState() =>
      _FavouriteCartoonsScreenState();
}

class _FavouriteCartoonsScreenState extends State<FavouriteCartoonsScreen> {
  FavouriteBloc bloc = FavouriteBloc();

  @override
  void dispose() {
    super.dispose();
    bloc.close();
  }

  @override
  Widget build(BuildContext context) {
    Color primaryColor = Theme.of(context).primaryColor;
    Color accentColor = Theme.of(context).accentColor;
    bloc.add(OnLoad());
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
            elevation: 0,
            title: Text('Favourite'),
            backgroundColor: Colors.white,
          ),
          drawer: NavigationDrawer(),
          body: BlocBuilder<FavouriteBloc, FavouriteState>(
            bloc: bloc,
            builder: (context, state) {
              if (state is FavouriteLoading) {
                List<Cartoon> cartoons = state.cartoons;
                List<Widget> widgets =
                    buildCartoons(context: context, cartoons: cartoons);
                widgets.add(Center(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: CircularProgressIndicator(),
                  ),
                ));
                return BlocProvider(
                  create: (context) => bloc,
                  child: FavouriteCartoonCarousel(
                    elements: widgets,
                  ),
                );
              } else if (state is FavouriteComplete) {
                List<Cartoon> cartoons = state.cartoons;
                List<Widget> widgets = buildCartoons(
                    context: context, cartoons: cartoons, displayStar: false);
                return BlocProvider(
                  create: (context) => bloc,
                  child: FavouriteCartoonCarousel(
                    elements: widgets,
                  ),
                );
              }
              return Container(
                height: 0,
                width: 0,
              );
            },
          )),
    );
  }
}

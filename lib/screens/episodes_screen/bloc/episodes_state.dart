part of 'episodes_bloc.dart';

@immutable
abstract class EpisodesState {}

class EpisodesLoading extends EpisodesState{
  final List<Episode> episodes;
  EpisodesLoading({@required this.episodes});
}

class EpisodesComplete extends EpisodesState{
  final List<Episode> episodes;
  EpisodesComplete({@required this.episodes});
}

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:childvids2/data_structures/Cartoon.dart';
import 'package:childvids2/data_structures/Episode.dart';
import 'package:childvids2/services/api_service.dart';
import 'package:meta/meta.dart';

part 'episodes_event.dart';
part 'episodes_state.dart';

class EpisodesBloc extends Bloc<EpisodesEvent, EpisodesState> {
  Cartoon cartoon;
  List<Episode> episodes;
  int loaded = 0;
  final int episodesPerBatch = 10;
  bool isLoading;

  EpisodesBloc({String channelId}){
    init(channelId: channelId);
  }

  void init({String channelId}) async{
    cartoon = await APIService.instance.fetchCartoon(channelId: channelId);
  }

  Future<List<Episode>> _fetchEpisodes() async{
    isLoading = true;
    List<Episode> episodes = await APIService.instance.fetchEpisodesFromPlaylist(playlistId: cartoon.uploadPlaylistUrl);
    loaded += episodesPerBatch;
    isLoading = false;
    return episodes;
  }

  @override
  Stream<EpisodesState> mapEventToState(EpisodesEvent event) async*{
    if(event is OnLoad){
      yield EpisodesLoading(episodes: cartoon.episodes);
      episodes = await _fetchEpisodes();
      yield EpisodesComplete(episodes: episodes);
    }
    else if (event is Overscroll){
      yield EpisodesLoading(episodes: cartoon.episodes);
      List<Episode> more = await _fetchEpisodes();
      episodes = episodes..addAll(more);
      yield EpisodesComplete(episodes: episodes);
    }
  }

  @override
  EpisodesState get initialState => EpisodesLoading(episodes: List<Episode>());
}
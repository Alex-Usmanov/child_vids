import 'package:childvids2/screens/WelcomeScreen.dart';
import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    home: WelcomeScreen(),
    debugShowCheckedModeBanner: false,
    theme: ThemeData(
      fontFamily: 'Montserrat',
      primaryColor: Color(0xFFF2994A),
      accentColor: Color(0xFFF2C94C),
    ),
  ));
}

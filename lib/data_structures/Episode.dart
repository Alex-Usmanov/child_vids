import 'package:childvids2/services/api_service.dart';
import 'package:flutter/cupertino.dart';


class Episode{
  final String title;
  final String previewUrl;
  final String videoId;
  bool isViewed = false;

  Episode({
    this.title,
    this.previewUrl,
    this.videoId,
    this.isViewed
  });

  factory Episode.fromMap(Map<String, dynamic> map){
    return Episode(
      videoId:  map['resourceId']['videoId'],
      title: map['title'],
      previewUrl: map['thumbnails']['high']['url'],
    );
  }
}

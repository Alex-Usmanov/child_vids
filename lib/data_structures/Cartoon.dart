import 'package:childvids2/data_structures/Episode.dart';

class Cartoon {
  String id;
  String profilePictureUrl;
  String subscriberCount;
  String videosCount;
  String uploadPlaylistUrl;
  String title;
  List<Episode> episodes;

  Cartoon(
      {this.id, this.title, this.profilePictureUrl, this.subscriberCount, this.videosCount, this.uploadPlaylistUrl, this.episodes});

  factory Cartoon.fromMap(Map<String, dynamic> map){
    return Cartoon(
        id: map['id'],
        title: map['snippet']['title'],
        profilePictureUrl: map['snippet']['thumbnails']['default']['url'],
        subscriberCount: map['statistics']['videoCount'],
        uploadPlaylistUrl: map['contentDetails']['relatedPlaylists']['uploads']
    );
  }

  Map<String, dynamic> toMap(){
    Map<String, String> result = {
      'id' : this.id,
      'title' : this.title,
      'profilePictureUrl' : this.profilePictureUrl,
      'subscriberCount' : this.subscriberCount,
      'uploadPlaylistUrl' : this.uploadPlaylistUrl,
    };
    return result;
  }

}
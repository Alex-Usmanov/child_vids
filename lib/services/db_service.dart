import 'dart:async';
import 'dart:core';
import 'dart:io';

import 'package:childvids2/data_structures/Cartoon.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseService {
  static final instance = DatabaseService._();

  DatabaseService._();

  static Database db;

  Future<Database> get database async {
    if (db != null){
     return db;
    }
    db = await init();
    return db;
  }

  Future<dynamic> init() async {
    String path = await getDatabasesPath() + 'cyka';
    print(path);
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute("CREATE TABLE Favourite("
          "id TEXT,"
          "title TEXT,"
          "profilePictureUrl TEXT,"
          "subscriberCount TEXT,"
          "videosCount TEXT,"
          "uploadPlaylistUrl TEXT"
          ")");
    });
  }

  Future<void> addCartoon(Cartoon cartoon) async {
    Database database = await this.database;
    database.insert('Favourite', cartoon.toMap());
  }

  Future<List<Cartoon>> fetchBatch(String table, int offset, int limit) async {
    Database database = await this.database;
    var response =
        await database.rawQuery('SELECT * from $table LIMIT $offset, $limit');
    if (response.isNotEmpty) {
      List<Cartoon> favourite = new List<Cartoon>();
      response.forEach((map) {
        Cartoon cartoon = new Cartoon();
        cartoon.id = map['id'];
        cartoon.title = map['title'];
        cartoon.uploadPlaylistUrl = map['uploadPlaylistUrl'];
        cartoon.profilePictureUrl = map['profilePictureUrl'];

        favourite.add(cartoon);
      });
      return favourite;
    }
    else{

      return new List<Cartoon>();
    }
  }

  Future<void> deleteCartoon(String table, Cartoon cartoon) async{
    Database database = await this.database;
    database.delete(table, where: 'id = ?', whereArgs: [cartoon.id]);
  }
}

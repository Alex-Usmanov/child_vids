import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'package:childvids2/data_structures/Cartoon.dart';
import 'package:childvids2/data_structures/Episode.dart';
import 'package:childvids2/keys.dart';

// ty marcus :)

class APIService {
  final String baseUrl = 'www.googleapis.com';
  String nextPageToken = '';
  String nextPageChannelToken = '';
  static final APIService instance = APIService._instantiate();

  APIService._instantiate();

  Future<Cartoon> fetchCartoon({String channelId}) async {
    Map<String, String> parameters = {
      'part': 'snippet, contentDetails, statistics',
      'id': channelId,
      'key': YOUTUBE_API_KEY,
    };

    Uri uri = new Uri.https(baseUrl, '/youtube/v3/channels', parameters);

    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'application/json',
    };

    var response = await http.get(uri, headers: headers);
    if (response.statusCode == 200) {
      // Success
      Map<String, dynamic> data = json.decode(response.body)['items'][0];
      Cartoon cartoon = Cartoon.fromMap(data);

      return cartoon;
    } else {
      throw json.decode(response.body)['error']['message'];
    }
  }

  Future<List<Episode>> fetchEpisodesFromPlaylist({playlistId}) async {
    Map<String, String> parameters = {
      'part': 'snippet',
      'playlistId': playlistId,
      'maxResults': '10',
      'pageToken': nextPageToken,
      'key': YOUTUBE_API_KEY,
    };

    Uri uri = Uri.https(baseUrl, '/youtube/v3/playlistItems', parameters);

    Map<String, String> headers = {
      HttpHeaders.contentTypeHeader: 'applicaiton/json',
    };

    var response = await http.get(uri, headers: headers);
    if (response.statusCode == 200) {
      Map<String, dynamic> data = json.decode(response.body);
      nextPageToken = data['nextPageToken'] ?? '';
      List<dynamic> videosJson = data['items'];
      List<Episode> episodes = [];

      videosJson
          .forEach((json) => episodes.add(Episode.fromMap(json['snippet'])));
      return episodes;
    } else
      throw json.decode(response.body)['error']['message'];
  }

  Future<List<Cartoon>> searchCartoons({@required String query}) async {
    Map<String, String> parameters = {
      'q': query,
      'part': 'snippet',
      'maxResults': '5',
      'order': 'relevance',
      'pageToken': nextPageChannelToken,
      'safeSearch': 'strict',
      'type': 'channel',
      'key': YOUTUBE_API_KEY,
    };

    Uri uri = Uri.https(baseUrl, '/youtube/v3/search/', parameters); //

    var response = await http.get(uri);
    List<Cartoon> cartoons = new List<Cartoon>();

    if (response.statusCode == 200) {
      Map<String, dynamic> data = json.decode(response.body);
      nextPageChannelToken = data['nextPageToken'];
      List<dynamic> channels = data['items'];
      print(channels[0]);
      // title : map['id']['snippet']['title']
      // thumbnail = map['id']['snippet']['thumbnails']['medium']
      // channelId: channel['id']['channelId'])
      for (var channel in channels) {
        Cartoon cartoon = new Cartoon(
            title: channel['id']['snippet']['title'],
            profilePictureUrl: channel['id']['snippet']['thumbnails']['medium'],
            id: channel['id']['channelId']);
        cartoons.add(cartoon);
      }
      return cartoons;
    } else {
      throw json.decode(response.body)['error']['message'];
    }
  }
}
